# Notes about REST, Hibernate 

## Database naming convention and strategy

In out implementation, the database tables are all in snake case *foo_bar*, 
corresponding to their camel case entity *FooBar*. And all the columns
in the database are named *bar_foo* instead of using directly the field name
*barFoo*

We achieve this by setting the Hibernate property in *persistence.xml*
```
<property name="hibernate.physical_naming_strategy" value="it.dreamhack.util.SnakeCaseNamingStrategy" />
```
where the *SnakeCaseNamingStrategy.java* is defined by ourselves.

But, it must be noted that, in the `@NamedQuery` **HQL** we are not using the
real physical database name, but still the camel case ones.

## REST service

To use 
