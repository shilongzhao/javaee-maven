package it.dreamhack.university;

import it.dreamhack.course.model.Course;
import it.dreamhack.university.model.University;
import it.dreamhack.university.service.UniversityService;
import it.dreamhack.university.service.UniversityServiceBean;
import it.dreamhack.util.SnakeCaseNamingStrategy;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@RunWith(Arquillian.class)
public class UniversityRepositoryTest {

    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, "university.war")
                .addClasses( UniversityService.class, UniversityServiceBean.class, SnakeCaseNamingStrategy.class )
                .addPackages(true,"it.dreamhack.university", "it.dreamhack.course")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                // Deploy our authentication datasource
                .addAsWebInfResource("test-ds.xml", "test-ds.xml");
    }
    @Inject
    private UniversityService us;
    @Resource
    private UserTransaction userTransaction;
    @PersistenceContext
    private EntityManager em;

    @Before
    public void prepare() throws Exception {
        clean();
        insertUniversity();
        startTx();
    }

    @After
    public void commitTx() throws Exception {
        userTransaction.commit();
    }

    private void startTx() throws Exception {
        userTransaction.begin();
        em.joinTransaction();
    }

    private void insertUniversity() throws Exception {
        userTransaction.begin();
        em.joinTransaction();
        System.out.println("Inserting records ... ");

        for (int i = 0; i < 5; i++) {
            University po = new University();
            po.setFullName("University " + i);
            po.setHomepage("Homepage " + i);
            po.setStudentEmailPostfix("Student Email " + i);
            po.setTeacherEmailPostfix("Teacher Email " + i);
            em.persist(po);
            Course c = new Course();
            c.setUniversity(po);
            c.setDescription("Course description " + i);
            c.setTitle("Course " + i);
            c.setCode("CS0" + i);
            em.persist(c);
        }

        userTransaction.commit();

        em.clear();
    }

    public void clean() throws Exception {
        userTransaction.begin();
        em.joinTransaction();
        System.out.println("Dumping old records ... ");
        em.createQuery("DELETE FROM University").executeUpdate();
        userTransaction.commit();
    }

    @Test
    public void shouldGetContent() {
        List<University> universities = us.read(0, 10);
        assertTrue(universities.size() == 5);
        assertTrue(universities.get(0).getHomepage().equals("Homepage 0"));

        University u = us.read(1L);
        assertTrue(u.getHomepage().equals("Homepage 0"));
    }
}