package it.dreamhack.course;

import it.dreamhack.course.model.Course;
import it.dreamhack.course.service.CourseService;
import it.dreamhack.university.model.University;
import it.dreamhack.university.service.UniversityService;
import it.dreamhack.util.SnakeCaseNamingStrategy;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@RunWith(Arquillian.class)
public class CourseServiceTest {
    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "course.war")
                .addClasses(SnakeCaseNamingStrategy.class)
                .addPackages(true,"it.dreamhack.university", "it.dreamhack.course")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("test-ds.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    CourseService cs;
    @Inject
    UniversityService us;
    @Resource
    UserTransaction utx;
    @PersistenceContext
    EntityManager em;

    @Before
    public void prepare() throws Exception {
        clean();
        insertData();
        startTx();
    }

    @After
    public void commitTx() throws Exception {
        utx.commit();
    }

    private void startTx() throws Exception {
        utx.begin();
        em.joinTransaction();
    }

    private void insertData() throws Exception {
        utx.begin();
        em.joinTransaction();
        System.out.println("Inserting records ... ");
        University u = new University();
        u.setHomepage("www.polito.it");
        u.setFullName("Politecnico di Torino");
        u.setStudentEmailPostfix("@studenti.polito.it");
        u.setTeacherEmailPostfix("@polito.it");
        us.create(u);

        Course c = new Course();
        c.setCode("CS01");
        c.setTitle("Introduction to Programming Language");
        c.setDescription("Description CS01");
        c.setUniversity(u);
        cs.create(c);

        Course j = new Course();
        j.setCode("CS02");
        j.setTitle("Object Oriented Programming");
        j.setDescription("Description CS02");
        j.setUniversity(u);
        cs.create(j);


        utx.commit();
        em.clear();
    }

    public void clean() throws Exception {
        utx.begin();
        em.joinTransaction();
        System.out.println("Dumping old records ... ");
        em.createQuery("DELETE FROM Course ").executeUpdate();
        utx.commit();
    }


    @Test
    public void testCourse() {
        List<Course> courses = cs.read(0, 10);
        assertTrue(2 == courses.size());
        Course c = cs.read("CS01");
        assertTrue(null != c.getUniversity());
        Course j = cs.read("CS02");
        assertTrue(null != j.getUniversity());
        List<Course> pCourses = us.getCourses(c.getUniversity().getId());
        assertTrue(2 == pCourses.size());
    }
}
