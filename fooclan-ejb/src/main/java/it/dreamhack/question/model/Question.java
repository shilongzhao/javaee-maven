package it.dreamhack.question.model;

import it.dreamhack.course.model.Course;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Entity
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    @Size(min = 1, max = 150)
    private String title;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private boolean solutionFree = true;

    /**
     * The user has to complete this function prototype
     * and then his code will be submitted for judge
     */
    @Column(nullable = false)
    private String functionDeclaration;

    /**
     * This is the reference answer, which can be proprietary,
     * that is, the user has to pay to view this
     */
    private String referenceSolution;

    @ManyToOne
    private Course course;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getFunctionDeclaration() {
        return functionDeclaration;
    }

    public void setFunctionDeclaration(String functionDeclaration) {
        this.functionDeclaration = functionDeclaration;
    }

    public String getReferenceSolution() {
        return referenceSolution;
    }

    public void setReferenceSolution(String referenceSolution) {
        this.referenceSolution = referenceSolution;
    }

    public boolean isSolutionFree() {
        return solutionFree;
    }

    public void setSolutionFree(boolean solutionFree) {
        this.solutionFree = solutionFree;
    }
}
