package it.dreamhack.question.dto;

import it.dreamhack.course.dto.CourseSummaryDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class QuestionDetailRequestDto {
    private String title;
    private String description;
    private boolean solutionFree;
    private String functionDeclaration;
    private String referenceSolution;
    private CourseSummaryDto course;
}
