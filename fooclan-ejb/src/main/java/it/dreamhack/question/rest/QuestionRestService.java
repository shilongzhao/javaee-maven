package it.dreamhack.question.rest;

import it.dreamhack.question.dto.QuestionDetailRequestDto;

import javax.ejb.Local;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Local
@Path("/questions")
@Produces(MediaType.APPLICATION_JSON)
public interface QuestionRestService {

    @GET
    Response read(@QueryParam("offset") int offset, @QueryParam("count") int count);

    @GET
    @Path("{title}")
    Response read(@PathParam("title") String title);

    @POST
    Response create(@Valid QuestionDetailRequestDto dto);

    @PUT
    @Path("{title}")
    Response update(@Valid QuestionDetailRequestDto dto);

    @DELETE
    @Path("{title}")
    Response delete();
}
