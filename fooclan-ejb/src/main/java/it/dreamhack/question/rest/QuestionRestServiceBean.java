package it.dreamhack.question.rest;

import it.dreamhack.question.dto.QuestionDetailRequestDto;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Stateless
public class QuestionRestServiceBean implements QuestionRestService {
    @Override
    public Response read(int offset, int count) {
        return null;
    }

    @Override
    public Response read(String title) {
        return null;
    }

    @Override
    public Response create(QuestionDetailRequestDto dto) {
        return null;
    }

    @Override
    public Response update(QuestionDetailRequestDto dto) {
        return null;
    }

    @Override
    public Response delete() {
        return null;
    }
}
