package it.dreamhack.question.service;

import it.dreamhack.question.model.Question;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Stateless
public class QuestionServiceBean implements QuestionService, Serializable {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Question read(Long questionId) {
        return em.createQuery(
                "SELECT q " +
                        "FROM Question q " +
                        "WHERE q.id = :id", Question.class).
                setParameter("id", questionId).
                getSingleResult();
    }

    @Override
    public Question read(String title) {
        return em.createQuery(
                "SELECT q " +
                        "FROM Question q " +
                        "WHERE q.title = :title", Question.class).
                setParameter("title", title).
                getSingleResult();
    }

    @Override
    public List<Question> search(Question obj, int pageSize, int pageNum) {
        return em.createQuery(
                "SELECT q " +
                        "FROM Question AS q " +
                        "WHERE (q.id = :id or :id is null) " +
                        "   AND (q.title = :title or :title is null) " +
                        "   AND (q.course = :course or :course is null)", Question.class)
                .setParameter("id", obj.getId())
                .setParameter("title", obj.getTitle())
                .setParameter("course", obj.getCourse())
                .setMaxResults(pageSize).
                setFirstResult((pageNum - 1) * pageSize)
                .getResultList();
    }

}
