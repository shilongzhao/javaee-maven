package it.dreamhack.question.service;

import it.dreamhack.question.model.Question;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Local
public interface QuestionService {
    Question read(Long questionId);
    Question read(String title);
    List<Question> search(Question obj, int pageSize, int pageNum);
}
