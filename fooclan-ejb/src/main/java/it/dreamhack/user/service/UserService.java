package it.dreamhack.user.service;


import it.dreamhack.role.model.Role;
import it.dreamhack.user.model.User;

import javax.ejb.Local;
import java.util.List;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
@Local
public interface UserService {
    Long count();
    void persist(User user);
    /* only User related operations here, common CRUD operations are in GenericService */
    User findByUsername(String username);
    User findByEmail(String email);
    List<Role> getUserRoles(String username);
    List<User> search(User obj, int pageSize, int pageNum);
}
