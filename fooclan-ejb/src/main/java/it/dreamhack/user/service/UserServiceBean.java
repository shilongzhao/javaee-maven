package it.dreamhack.user.service;

import it.dreamhack.role.model.Role;
import it.dreamhack.role.model.RoleProfile;
import it.dreamhack.role.service.RoleService;
import it.dreamhack.user.model.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
@Stateless
public class UserServiceBean implements UserService, Serializable {

    @PersistenceContext
    private EntityManager em;

    @Inject
    private RoleService rs;

    @Override
    public List<User> search(User obj, int pageSize, int pageNum) {
        return em.createQuery("SELECT u " +
                "FROM User u " +
                "WHERE " +
                "       (u.id = :id or :id is null) " +
                "   AND (:username is null or u.username like concat('%',:username,'%')) " +
                "   AND (:email is null or u.email like concat('%', :email, '%')) ", User.class).
                setParameter("id", obj.getId()).
                setParameter("username", obj.getUsername()).
                setParameter("email", obj.getEmail()).
                setMaxResults(pageSize).
                setFirstResult((pageNum - 1) * pageSize).
                getResultList();
    }

    public User findByUsername(String username) {
        return em.createQuery(
                "SELECT u " +
                        "FROM User u " +
                        "   JOIN FETCH u.roles " +
                        "WHERE u.username = :username", User.class).
                setParameter("username", username).
                getSingleResult();
    }

    public User findByEmail(String email) {
        User u = new User();
        u.setEmail(email);
        List<User> r = search(u, 0, 0);
        return r.get(0);
    }

    public List<Role> getUserRoles(String username) {
        return em.createQuery(
                     "SELECT r " +
                        "   FROM User u " +
                        "   JOIN u.roles r " +
                        "WHERE " +
                        "   u.username = :username",
                Role.class).
                setParameter("username", username).
                getResultList();
    }

    @Override
    public Long count() {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        cq.select(qb.count(cq.from(User.class)));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public void persist(User user) {
        user.setEnabled(true);

        if (user.getRoles().size() == 0) {
            user.addRole(rs.getRole(RoleProfile.USER));
        }

        em.persist(user);
    }
}
