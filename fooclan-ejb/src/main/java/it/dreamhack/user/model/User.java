package it.dreamhack.user.model;

import it.dreamhack.role.model.Role;
import it.dreamhack.university.model.University;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */

@NamedQueries({
        @NamedQuery(name = "User.search", query = //HQL query
                                "SELECT u " +
                                "FROM User AS u JOIN u.roles " +
                                "WHERE " +
                                "       (u.id = :id or :id is null) " +
                                "   AND (:username is null or u.username like concat('%',:username,'%')) " +
                                "   AND (:email is null or u.email like concat('%', :email, '%')) "
        ),
        @NamedQuery(name = "User.findByUsername", query =
                                "SELECT u " +
                                "FROM User AS u " +
                                "WHERE u.username = :username "
        )
    }

)
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlAttribute
    private Long id;

    @Column(unique = true, nullable = false)
    @NotNull
    private String username;

    @Column(unique = true, nullable = false)
    @Email @NotNull
    private String email;

    @ManyToOne
    private University university;

    //TODO: hide password when read user, declare UserDTO.java ?
    @NotNull
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name ="user_role", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
                                    inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles = new ArrayList<>();

    @Column(nullable = false, columnDefinition = "TINYINT", length = 1)
    private Boolean enabled;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDatetime;

    @Version
    private Timestamp version;

    public User() { }

    @PrePersist
    protected void onCreate() {
        creationDatetime = new Date();
    }

    @PreUpdate
    protected  void onUpdate() {
        version = new Timestamp(System.currentTimeMillis());
    }

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Timestamp getVersion() {
        return version;
    }

    public void setVersion(Timestamp version) {
        this.version = version;
    }

    public Date getCreationDatetime() {
        return creationDatetime;
    }

    public List<Role> getRoles() {
        return roles;
    }

//    public void setRoles(List<Role> roles) {
//        this.roles = roles;
//    }

    public void addRole(Role role) {
        roles.add(role);
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public void setCreationDatetime(Date creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public String toString() {
        return String.format("User: [id: %d, username: %s]", id, username);
    }

}
