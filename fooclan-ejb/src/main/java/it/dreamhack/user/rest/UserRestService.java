package it.dreamhack.user.rest;

import it.dreamhack.user.model.User;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.authz.annotation.RequiresUser;

import javax.ejb.Local;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Local
@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public interface UserRestService {
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @RequiresUser
    Response getUser(@PathParam("id") Long id);

    @POST
    @RequiresRoles("ADMIN")
    Response addUser(@Valid User user, @Context UriInfo info);

    @POST
    @Path("/search")
    @RequiresRoles("ADMIN")
    Response searchUsers(User user, @QueryParam("pageSize") int pageSize, @QueryParam("pageNum") int pageNum);
}
