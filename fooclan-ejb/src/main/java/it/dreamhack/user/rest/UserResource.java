package it.dreamhack.user.rest;

import it.dreamhack.user.model.User;
import it.dreamhack.security.BCryptPasswordService;
import it.dreamhack.user.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.subject.Subject;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;


import javax.validation.Valid;
import javax.ws.rs.core.UriInfo;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
@Stateless
public class UserResource implements UserRestService{
    @Inject
    private UserService userService;
    @Inject
    private BCryptPasswordService passwordService;

    private Subject subject = SecurityUtils.getSubject();

    public Response getUser(Long id) {
        String username = (String) subject.getPrincipal();
        User currentUser = userService.findByUsername(username);
        if (id != currentUser.getId()) {
            return Response.status(Response.Status.FORBIDDEN).entity("Operation not allowed").build();
        }
        return Response.ok(currentUser).build();
    }

    public Response addUser(User user, UriInfo info) {
        String encryptPassword = passwordService.encryptPassword(user.getPassword());
        user.setPassword(encryptPassword);
        this.userService.persist(user);
        long id = user.getId();
        URI uri = info.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).build();
    }


//    @PUT
//    @RequiresUser
//    public Response updateUser(@Valid User user, @Context UriInfo info) {
//        User searched = this.userService.update(user);
//        return Response.ok(searched).build();
//    }

    public Response searchUsers(User user, int pageSize, int pageNum) {
        List<User> users = userService.search(user, pageSize, pageNum);
        return Response.ok(users).build();
    }
}
