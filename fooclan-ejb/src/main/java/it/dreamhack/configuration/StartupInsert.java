package it.dreamhack.configuration;

import it.dreamhack.role.model.Role;
import it.dreamhack.role.model.RoleProfile;
import it.dreamhack.user.model.User;
import it.dreamhack.security.BCryptPasswordService;
import it.dreamhack.user.service.UserService;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Startup
@Singleton
public class StartupInsert {
    @Inject
    private UserService userService;
    @Inject
    private BCryptPasswordService passwordService;

    @PostConstruct
    public void init() {
        User u = new User("shilong", "s.zhao@opengate.biz",
                passwordService.encryptPassword("hello123!"));

        Role r = new Role();
        r.setRoleProfile(RoleProfile.ADMIN);
        u.addRole(r);
        userService.persist(u);
    }
}
