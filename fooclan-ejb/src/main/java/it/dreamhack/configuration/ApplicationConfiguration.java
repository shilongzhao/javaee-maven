package it.dreamhack.configuration;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Singleton
@Startup
public class ApplicationConfiguration {
    private Properties properties;

    @PostConstruct
    public void init() {
        InputStream inputStream = this.getClass().getClassLoader().
                getResourceAsStream("application.properties");
        properties = new Properties();
        try{
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the configuration property
     * @param key
     * @return
     */
    public String get(String key) {
        return properties.getProperty(key);
    }
}
