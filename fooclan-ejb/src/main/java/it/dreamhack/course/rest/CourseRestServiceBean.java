package it.dreamhack.course.rest;

import it.dreamhack.course.dto.CourseDetailRequestDto;
import it.dreamhack.course.dto.CourseDetailResponseDto;
import it.dreamhack.course.dto.CourseListDto;
import it.dreamhack.course.model.Course;
import it.dreamhack.course.service.CourseService;
import it.dreamhack.university.model.University;
import it.dreamhack.university.service.UniversityService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Stateless
public class CourseRestServiceBean implements CourseRestService {

    @Inject
    private CourseService cs;
    @Inject
    private UniversityService us;

    @Override
    public Response read(int start, int maxResult) {
        List<Course> result = cs.read(start, maxResult);
        CourseListDto dto = new CourseListDto(result);
        return Response.ok(dto).build();
    }

    @Override
    public Response read(String code) {
       Course course =  cs.read(code);
       CourseDetailResponseDto dto = new CourseDetailResponseDto(course);
       return Response.ok(dto).build();
    }

    @Override
    public Response create(CourseDetailRequestDto requestDto) {
        Course course = new Course(requestDto);

        University u = us.read(requestDto.getUniversityName());
        course.setUniversity(u);

        cs.create(course);
        return Response.ok().build();
    }

    @Override
    public Response update(String code, CourseDetailRequestDto requestDto) {
        Course course = cs.read(code);

        University u = us.read(requestDto.getUniversityName());
        course.setUniversity(u);
        course.setTitle(requestDto.getTitle());
        course.setDescription(requestDto.getDescription());

        cs.update(course);
        return Response.ok().build();
    }

}
