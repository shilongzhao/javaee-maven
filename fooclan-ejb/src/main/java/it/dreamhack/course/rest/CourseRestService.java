package it.dreamhack.course.rest;

import it.dreamhack.course.dto.CourseDetailRequestDto;

import javax.ejb.Local;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Local
@Path("/courses")
@Produces(MediaType.APPLICATION_JSON)
public interface CourseRestService {

    @GET
    Response read(@QueryParam("start") int start, @QueryParam("maxResults") int maxResults);

    @GET
    @Path("/{code:[a-zA-Z0-9]+}")
    Response read(@PathParam("code") String code);

    /**
     * create new course
     * @param requestDto
     * @return
     */
    @POST
    Response create(@Valid CourseDetailRequestDto requestDto);

    /**
     * update a specific course information
     * @param requestDto user update request
     * @return
     */
    @PUT
    @Path("/{code:[a-zA-Z0-9]+}")
    Response update(@PathParam("code") String code, @Valid CourseDetailRequestDto requestDto);
}
