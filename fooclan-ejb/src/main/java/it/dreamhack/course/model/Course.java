package it.dreamhack.course.model;

import it.dreamhack.course.dto.CourseDetailRequestDto;
import it.dreamhack.university.model.University;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Entity
public class Course {
    @Id
    private String code; // Note the primary key is code not id

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private University university; // automatically mapped to university_id;

    @NotNull
    private String title;

    private String description;

    public Course() {
        super();
    }
    public Course(CourseDetailRequestDto requestDto) {
        super();
        this.code = requestDto.getCode();
        this.title = requestDto.getTitle();
        this.description = requestDto.getDescription();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return code;
    }
}
