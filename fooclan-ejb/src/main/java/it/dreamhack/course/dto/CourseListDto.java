package it.dreamhack.course.dto;

import it.dreamhack.course.model.Course;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CourseListDto {
    private int count;
    private List<CourseSummaryDto> courses = new ArrayList<>();

    public CourseListDto() {
        super();
    }

    public CourseListDto(List<Course> inputs) {
        super();
        for (Course c: inputs) {
            courses.add(new CourseSummaryDto(c));
        }
        count = courses.size();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void addCourse(CourseSummaryDto dto) {
        count++;
        courses.add(dto);
    }
}
