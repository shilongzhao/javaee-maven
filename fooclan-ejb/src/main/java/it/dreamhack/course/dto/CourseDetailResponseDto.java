package it.dreamhack.course.dto;

import it.dreamhack.course.model.Course;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CourseDetailResponseDto {
    private String code;
    private String title;
    private String description;
    private String universityName;
    private String detailUrl;

    public CourseDetailResponseDto(Course course) {
        code = course.getCode();
        title = course.getTitle();
        description = course.getDescription();
        universityName = course.getUniversity() == null? null : course.getUniversity().getFullName();
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }
}
