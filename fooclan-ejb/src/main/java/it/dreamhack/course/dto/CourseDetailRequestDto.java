package it.dreamhack.course.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 * For create/update/search Course
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CourseDetailRequestDto {
    @NotNull
    private String code;
    @NotNull
    private String title;
    private String description;
    @NotNull
    private String universityName;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
}
