package it.dreamhack.course.dto;

import it.dreamhack.course.model.Course;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CourseSummaryDto {
    private String code;
    private String universityName;
    private String title;
    private String description;

    public CourseSummaryDto(Course course) {
        if (course.getUniversity() != null) {
            universityName = course.getUniversity().getFullName();
        }
        this.code = course.getCode();
        this.title = course.getTitle();
        this.description = course.getDescription();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
