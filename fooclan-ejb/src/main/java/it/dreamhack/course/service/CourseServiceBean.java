package it.dreamhack.course.service;

import it.dreamhack.course.model.Course;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Stateless
public class CourseServiceBean implements CourseService {
    private Logger log = Logger.getLogger(CourseServiceBean.class.getCanonicalName());
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Course> read(int start, int maxResults) {
        return em.createQuery(
                "FROM Course", Course.class).
                setFirstResult(start).
                setMaxResults(maxResults).
                getResultList();
    }

    @Override
    public Course read(String code) {
        log.info("getting course for code " + code);
        return em.createQuery(
                "SELECT c " +
                        "FROM Course c " +
                        "JOIN FETCH c.university " +
                        "WHERE c.code = :code", Course.class).
                setParameter("code", code).getSingleResult();
    }

    @Override
    public void create(Course course) {
        em.persist(course);
    }

    @Override
    public void update(Course course) {
        em.merge(course);
    }
}
