package it.dreamhack.course.service;

import it.dreamhack.course.model.Course;

import javax.ejb.Local;
import java.util.List;


/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Local
public interface CourseService {
    List<Course> read(int start, int maxResults);
    Course read(String code);

    void create(Course course);

    void update(Course course);
}
