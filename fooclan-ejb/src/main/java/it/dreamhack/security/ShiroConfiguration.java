package it.dreamhack.security;

import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.web.filter.authc.AnonymousFilter;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.filter.authc.UserFilter;
import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.FilterChainManager;
import org.apache.shiro.web.filter.mgt.FilterChainResolver;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;

import javax.enterprise.inject.Produces;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
public class ShiroConfiguration {
// TODO: write user information to cookie (must be encrypted), e.g. user id, user name, subscribed or not, etc. And relieve the server

    @Produces
    public WebSecurityManager getSecurityManager() {
        DefaultWebSecurityManager securityManager = null;
        if (securityManager == null) {
            AuthorizingRealm realm = new SecurityRealm();

            CredentialsMatcher credentialsMatcher = new PasswordMatcher();
            ((PasswordMatcher) credentialsMatcher).setPasswordService(new BCryptPasswordService());
            realm.setCredentialsMatcher(credentialsMatcher);

            securityManager = new DefaultWebSecurityManager(realm);

            CacheManager cacheManager = new EhCacheManager();
            ((EhCacheManager) cacheManager).setCacheManagerConfigFile("classpath:ehcache.xml");
            securityManager.setCacheManager(cacheManager);

            CookieRememberMeManager rememberMeManager = new RandomRememberMeManager();

            securityManager.setRememberMeManager(rememberMeManager);
        }
        return securityManager;
    }

    @Produces
    public FilterChainResolver getFilterChainResolver() {
        FilterChainResolver filterChainResolver = null;
        if (filterChainResolver == null) {
            RolesAuthorizationFilter roles = new RolesAuthorizationFilter();
            FormAuthenticationFilter authc = new FormAuthenticationFilter();
            AnonymousFilter anon = new AnonymousFilter();
            UserFilter user = new UserFilter();

            authc.setLoginUrl(WebPages.LOGIN_URL);
            user.setLoginUrl(WebPages.LOGIN_URL);

            FilterChainManager fcMan = new DefaultFilterChainManager();
            fcMan.addFilter("authc", authc);
            fcMan.addFilter("anon", anon);
            fcMan.addFilter("user", user);
            fcMan.addFilter("roles", roles);

            /* see Shiro documentation to know more about the createChain usages */
            /* TODO: change to permission-based instead of role-based auth when I have time */
            fcMan.createChain("/primeface.xhtml", "roles[ADMIN]");
            fcMan.createChain("/index.xhtml", "anon");
            fcMan.createChain("/css/**", "anon");
            fcMan.createChain("/api/**", "anon");
            fcMan.createChain(WebPages.LOGIN_URL, "authc");
            fcMan.createChain("/**", "user");


            PathMatchingFilterChainResolver resolver = new PathMatchingFilterChainResolver();
            resolver.setFilterChainManager(fcMan);
            filterChainResolver = resolver;
        }
        return filterChainResolver;
    }

}

