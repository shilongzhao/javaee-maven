package it.dreamhack.security;

import it.dreamhack.role.model.Role;
import it.dreamhack.user.model.User;
import it.dreamhack.user.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;


/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */

public class SecurityRealm extends AuthorizingRealm {
    private UserService userService; // Injection does not work here

    private Logger logger;
    public SecurityRealm() {
        super();
        this.logger = Logger.getLogger(SecurityRealm.class.getName());
        try {
            InitialContext context = new InitialContext();
            String moduleName = (String) context.lookup("java:module/ModuleName");
            String appName = (String) context.lookup("java:app/AppName");
            this.userService = (UserService) context.lookup(String.format("java:global/%s/%s/UserServiceBean", appName, moduleName));
        } catch (NamingException ex) {
            logger.warning("Cannot do the JNDI lookup to instantiate the UserService: " + ex);
        }

    }
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }

        String username = (String) getAvailablePrincipal(principals);
        Set<String> roleNames = new HashSet<>();
        List<Role> userRoles = userService.getUserRoles(username);
        for (Role r: userRoles) {
            roleNames.add(r.getRoleProfile().getProfileName());
        }
        AuthorizationInfo info = new SimpleAuthorizationInfo(roleNames);
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;
        String username = userPassToken.getUsername();
        if (username == null) {
            logger.warning("Username is null.");
            return null;
        }
        User user = this.userService.findByUsername(username);

        if (user == null) {
            logger.warning("No account found for user [" + username + "]");
            throw new IncorrectCredentialsException();
        }

        return new SimpleAuthenticationInfo(username, user.getPassword(), getName());
    }
}
