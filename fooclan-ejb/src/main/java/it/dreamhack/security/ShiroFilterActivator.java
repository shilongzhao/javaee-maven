package it.dreamhack.security;

import org.apache.shiro.web.servlet.ShiroFilter;

import javax.servlet.annotation.WebFilter;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 * Activate Shiro
 */
@WebFilter("/*")
public class ShiroFilterActivator extends ShiroFilter {
    private ShiroFilterActivator() {

    }
}
