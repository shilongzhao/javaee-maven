package it.dreamhack.security;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
public final class WebPages {
    //
    public static final String LOGIN_URL = "/login.html";
    public static final String DASHBOARD_URL = "/account/";
    public static final String HOME_URL = "/home.html";

//    @Produces
//    public Logger produceLogger(InjectionPoint ip) {
//        return Logger.getLogger(ip.getMember().getDeclaringClass().getName());
//    }
}
