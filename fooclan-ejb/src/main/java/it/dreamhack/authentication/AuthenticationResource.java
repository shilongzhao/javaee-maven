package it.dreamhack.authentication;

import it.dreamhack.user.model.User;
import it.dreamhack.security.BCryptPasswordService;
import it.dreamhack.security.WebPages;
import it.dreamhack.security.AuthenticationEventMonitor;
import it.dreamhack.security.AuthenticationEvent;
import it.dreamhack.user.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
@Path("auth")
@Produces(MediaType.APPLICATION_JSON)
public class AuthenticationResource {
    @Inject
    private Logger logger;

    @Inject
    private UserService userService;

    @Inject
    private Event<AuthenticationEvent> monitoring;

    @Inject
    private AuthenticationEventMonitor authenticationEventMonitor;

    @Inject
    private BCryptPasswordService passwordService;

    //TODO: recapcha non-robot validation
    @POST
    @Path("login")
    public Response login(@NotNull @FormParam("username") String username,
                          @NotNull @FormParam("password") String password,
                          @NotNull @FormParam("rememberMe") boolean rememberMe,
                          @Context HttpServletRequest request) {

        String savedPass = userService.findByUsername(username).getPassword();
        String encryptedPass = passwordService.encryptPassword(password);

        boolean justLogged = SecurityUtils.getSubject().isAuthenticated();

        try {
            UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
            SecurityUtils.getSubject().login(token);
        } catch (Exception e) {
            throw new IncorrectCredentialsException("Unknown user, please try again");
        }

        SavedRequest savedRequest = WebUtils.getAndClearSavedRequest(request);
        monitoring.fire(new AuthenticationEvent(username, AuthenticationEvent.Type.LOGIN));
        if (savedRequest != null) {
            return this.getRedirectResponse(savedRequest.getRequestUrl(), request);
        } else {
            if (justLogged) {
                return this.getRedirectResponse(WebPages.DASHBOARD_URL, request);
            }
            return this.getRedirectResponse(WebPages.HOME_URL, request);
        }
    }

    @GET
    @Path("logout")
    public Response logout(@Context HttpServletRequest request) {
        SecurityUtils.getSubject().logout();
        return this.getRedirectResponse(WebPages.HOME_URL, request);
    }

    @GET
    @Path("me")
    @RequiresUser
    public Response getSubjectInfo() {
        Subject subject = SecurityUtils.getSubject();
        if (subject != null && subject.isAuthenticated()) {
            User connectedUser = this.userService.findByUsername(subject.getPrincipal()
                    .toString());
            return Response.ok(connectedUser).build();
        } else {
            return Response.serverError()
                    .type(MediaType.TEXT_HTML)
                    .build();
        }
    }

    @GET
    @Path("users")
    @RequiresRoles("ADMIN")
    public Response getConnectedUsers() {
        List<String> connectedUsers = this.authenticationEventMonitor.getConnectedUsers();
        return Response.ok(connectedUsers).build();
    }


    private Response getRedirectResponse(String requestedPath, HttpServletRequest request) {
        String appName = request.getContextPath();

        String baseUrl = request.getRequestURL().toString().split(request.getRequestURI())[0] + appName;

        try {
            if (requestedPath.split(appName).length > 1) {
                baseUrl += requestedPath.split(appName)[1];
            } else {
                baseUrl += requestedPath;
            }

            return Response.seeOther(new URI(baseUrl)).build();
        } catch (URISyntaxException ex) {
            logger.log(Level.WARNING, "URL redirection failed for request[{0}] : {1}", new Object[]{request, ex});
            return Response.serverError().status(404)
                    .type(MediaType.TEXT_HTML).build();
        }
    }

}
