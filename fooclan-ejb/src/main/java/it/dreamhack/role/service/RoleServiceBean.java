package it.dreamhack.role.service;

import it.dreamhack.role.model.Role;
import it.dreamhack.role.model.RoleProfile;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
@Stateless
@Local(RoleService.class)
public class RoleServiceBean implements RoleService {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Role getRole(RoleProfile profile) {
        Query q = em.createQuery("SELECT r " +
                "FROM Role r " +
                "WHERE r.roleProfile = :roleProfile");
        q.setParameter("roleProfile", profile);
        return (Role) q.getSingleResult();
    }

}
