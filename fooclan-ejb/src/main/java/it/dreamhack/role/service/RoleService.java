package it.dreamhack.role.service;

import it.dreamhack.role.model.Role;
import it.dreamhack.role.model.RoleProfile;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
public interface RoleService {
    Role getRole(RoleProfile profile);
}
