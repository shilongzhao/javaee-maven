package it.dreamhack.role.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
@NamedQuery(name = "Role.findByName", query = "SELECT r FROM Role r WHERE r.roleProfile = :name")
@Entity
@XmlRootElement
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlAttribute
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleProfile roleProfile;

    public Role() {}
    public Role(RoleProfile profile) {
        this.roleProfile = profile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleProfile getRoleProfile() {
        return roleProfile;
    }

    public void setRoleProfile(RoleProfile roleProfile) {
        this.roleProfile = roleProfile;
    }
}
