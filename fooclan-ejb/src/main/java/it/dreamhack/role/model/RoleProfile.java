package it.dreamhack.role.model;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
public enum RoleProfile {
    USER("USER"),
    TEACHER("TEACHER"),
    DBA("DBA"),
    ADMIN("ADMIN");

    private String profileName;
    private RoleProfile(String profileName) {
        this.profileName = profileName;
    }
    public String getProfileName() {
        return profileName;
    }
}
