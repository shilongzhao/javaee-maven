
- the design principle is that: join operations should NOT be performed
when the query is issued for a list of entities, to prevent an 
excessive amount of join. That's the reason why even for @ManyToOne 
relation, we still use lazy fetch strategy.
- in list() or search() methods, do not initialize the lazy properties
and these two methods are called from the list view.
- but in findById() the lazy fields must be initialized, since 
it is called from a detail view
- Think carefully if it is really needed before applying @OneToMany;
It is actually only suitable for OneToFew situation. 
And we can still retrieve information from the child side by @ManyToOne
