package it.dreamhack.university.dto;

import it.dreamhack.university.model.University;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@XmlRootElement
@XmlAccessorType
public class UniversitySummaryDto {
    private Long id;
    private String fullName;
    private String detailUrl;

    public UniversitySummaryDto() {
        super();
    }

    public UniversitySummaryDto(University u) {
        super();
        this.id = u.getId();
        this.fullName = u.getFullName();
        this.detailUrl = "/courses/" + id;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        this.detailUrl = "/courses/" + id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

}
