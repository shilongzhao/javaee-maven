package it.dreamhack.university.dto;


import it.dreamhack.course.dto.CourseSummaryDto;
import it.dreamhack.course.model.Course;
import it.dreamhack.university.model.University;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UniversityDetailResponseDto {
    private Long id;
    private String fullName;
    private String studentEmailPostfix;
    private String teacherEmailPostfix;
    private String homepage;
    private List<CourseSummaryDto> courses = new ArrayList<>();

    public UniversityDetailResponseDto() {
        super();
    }
    public UniversityDetailResponseDto(University u) {
        super();
        this.id = u.getId();
        this.fullName = u.getFullName();
        this.studentEmailPostfix = u.getStudentEmailPostfix();
        this.teacherEmailPostfix = u.getTeacherEmailPostfix();
        this.homepage = u.getHomepage();

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStudentEmailPostfix() {
        return studentEmailPostfix;
    }

    public void setStudentEmailPostfix(String studentEmailPostfix) {
        this.studentEmailPostfix = studentEmailPostfix;
    }

    public String getTeacherEmailPostfix() {
        return teacherEmailPostfix;
    }

    public void setTeacherEmailPostfix(String teacherEmailPostfix) {
        this.teacherEmailPostfix = teacherEmailPostfix;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public List<CourseSummaryDto> getCourses() {
        return courses;
    }

    public void setCourses(List<CourseSummaryDto> courses) {
        this.courses = courses;
    }
}
