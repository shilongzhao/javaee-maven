package it.dreamhack.university.dto;

import it.dreamhack.university.model.University;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UniversityListDto {
    private List<UniversitySummaryDto> universityList = new ArrayList<>();

    public UniversityListDto() {
        super();
    }
    public UniversityListDto(List<University> universities) {
        super();
        for (University u: universities) {
            UniversitySummaryDto s = new UniversitySummaryDto(u);
            universityList.add(s);
        }
    }

    public List<UniversitySummaryDto> getUniversityList() {
        return universityList;
    }

    public void setUniversityList(List<UniversitySummaryDto> universityList) {
        this.universityList = universityList;
    }
}
