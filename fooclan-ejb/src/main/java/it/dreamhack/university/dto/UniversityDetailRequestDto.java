package it.dreamhack.university.dto;

import it.dreamhack.university.model.University;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UniversityDetailRequestDto {
    private String fullName;
    private String studentEmailPostfix;
    private String teacherEmailPostfix;
    private String homepage;

    public UniversityDetailRequestDto() {
        super();
    }
    public UniversityDetailRequestDto(University university) {
        super();
        this.fullName = university.getFullName();
        this.studentEmailPostfix = university.getStudentEmailPostfix();
        this.teacherEmailPostfix = university.getTeacherEmailPostfix();
        this.homepage = university.getHomepage();
    }
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStudentEmailPostfix() {
        return studentEmailPostfix;
    }

    public void setStudentEmailPostfix(String studentEmailPostfix) {
        this.studentEmailPostfix = studentEmailPostfix;
    }

    public String getTeacherEmailPostfix() {
        return teacherEmailPostfix;
    }

    public void setTeacherEmailPostfix(String teacherEmailPostfix) {
        this.teacherEmailPostfix = teacherEmailPostfix;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }
}
