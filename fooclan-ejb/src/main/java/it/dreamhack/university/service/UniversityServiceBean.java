package it.dreamhack.university.service;


import it.dreamhack.course.model.Course;
import it.dreamhack.university.model.University;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Stateless
public class UniversityServiceBean implements UniversityService {
    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(University u) {
        em.persist(u);
    }

    @Override
    public List<University> read(int offset, int count) {
        return em.createQuery("SELECT u FROM University u", University.class).
                setFirstResult(offset).
                setMaxResults(count).
                getResultList();
    }

    @Override
    public University read(Long id) {
        return em.createQuery("SELECT u " +
                "FROM University u " +
                "WHERE u.id = :id", University.class).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public University read(String fullName) {
        return em.createQuery(
                     "SELECT u " +
                        "FROM University u " +
                        "WHERE u.fullName = :name", University.class).
                setParameter("name", fullName).
                getSingleResult();
    }

    @Override
    public void update(University u) {
        em.merge(u);
    }

    @Override
    public void delete(Long id) {
    }

    @Override
    public void delete(String name) {
    }

    @Override
    public List<Course> getCourses(Long id) {
        return em.createQuery(
                "SELECT c " +
                        "FROM Course c " +
                        "WHERE c.university.id = :id", Course.class).
                setParameter("id", id).
                getResultList();
    }
}
