package it.dreamhack.university.service;


import it.dreamhack.course.model.Course;
import it.dreamhack.university.model.University;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Local
public interface UniversityService {
    void create(University u);

    List<University> read(int offset, int count);
    University read(Long id);
    University read(String fullName);

    void update(University u);

    void delete(Long id);
    void delete(String name);

    List<Course> getCourses(Long id);
}
