package it.dreamhack.university.model;


import it.dreamhack.university.dto.UniversityDetailRequestDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Entity
public class University {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String fullName;

    @Column(unique = true, nullable = false)
    private String studentEmailPostfix;

    @Column(unique = true, nullable = false)
    private String teacherEmailPostfix;

    @NotNull @Column(unique = true, nullable = false)
    private String homepage;

    public University() {
        super();
    }

    public University(UniversityDetailRequestDto dto) {
        super();
        this.fullName = dto.getFullName();
        this.studentEmailPostfix = dto.getStudentEmailPostfix();
        this.teacherEmailPostfix = dto.getTeacherEmailPostfix();
        this.homepage = dto.getHomepage();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStudentEmailPostfix() {
        return studentEmailPostfix;
    }

    public void setStudentEmailPostfix(String studentEmailPostfix) {
        this.studentEmailPostfix = studentEmailPostfix;
    }

    public String getTeacherEmailPostfix() {
        return teacherEmailPostfix;
    }

    public void setTeacherEmailPostfix(String teacherEmailPostfix) {
        this.teacherEmailPostfix = teacherEmailPostfix;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @Override
    public String toString() {
        return fullName;
    }
}
