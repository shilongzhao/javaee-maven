package it.dreamhack.university.rest;

import it.dreamhack.university.dto.UniversityDetailRequestDto;

import javax.ejb.Local;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Local
@Path("/universities")
@Produces(MediaType.APPLICATION_JSON)
public interface UniversityRestService {
    @GET
    Response read(@QueryParam("offset") int offset, @QueryParam("count") int count);

    @GET
    @Path("/{id:[0-9]+}")
    Response read(@PathParam("id") Long id);

    @POST
    Response create(@Valid UniversityDetailRequestDto dto);

    @PUT
    @Path("/{id:[0-9]+}")
    Response update(@PathParam("id") Long id, @Valid UniversityDetailRequestDto dto);

    @DELETE
    @Path("/{id:[0-9]+}")
    Response delete(@PathParam("id") Long id);
}
