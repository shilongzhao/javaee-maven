package it.dreamhack.university.rest;

import it.dreamhack.university.dto.UniversityDetailRequestDto;
import it.dreamhack.university.dto.UniversityListDto;
import it.dreamhack.university.model.University;
import it.dreamhack.university.service.UniversityService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
@Stateless
public class UniversityRestServiceBean implements UniversityRestService {
    @Inject
    UniversityService us;

    @Override
    public Response read(int offset, int count) {
        List<University> universities = us.read(offset, count);
        UniversityListDto result = new UniversityListDto(universities);
        return Response.ok(result).build();
    }

    @Override
    public Response read(Long id) {
        University u = us.read(id);
        return Response.ok(u).build();
    }

    @Override
    public Response create(UniversityDetailRequestDto dto) {
        University u = new University(dto);
        us.create(u);
        return Response.ok().build();
    }

    @Override
    public Response update(Long id, UniversityDetailRequestDto dto) {
        University u = new University(dto);
        u.setId(id);
        us.update(u);
        return Response.ok().build();
    }

    @Override
    public Response delete(Long id) {
        return Response.status(Response.Status.NOT_FOUND).build();
    }

}
