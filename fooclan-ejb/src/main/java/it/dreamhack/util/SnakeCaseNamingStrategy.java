package it.dreamhack.util;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import java.util.Locale;

import static java.lang.Character.*;
/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 * Customize the entity column mapping from property camelCase to snake_case
 * Remember to configure the Hibernate properties
 */
public class SnakeCaseNamingStrategy extends PhysicalNamingStrategyStandardImpl {
    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        return new Identifier(camelCaseToLowerUnderscore(name.getText()), name.isQuoted());
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        return new Identifier(camelCaseToLowerUnderscore(name.getText()), name.isQuoted());
    }

    private static String camelCaseToLowerUnderscore(String camel) {
        final StringBuilder buf = new StringBuilder(camel);
        for (int i = 1; i < buf.length(); i++) {
            if (isLowerCase(buf.charAt(i-1)) && isUpperCase(buf.charAt(i)) && isLowerCase(buf.charAt(i+1))) {
                buf.insert(i++, '_');
            }
        }
        return buf.toString().toLowerCase(Locale.ROOT);
    }
}
