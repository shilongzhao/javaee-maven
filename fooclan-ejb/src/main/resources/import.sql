--
-- JBoss, Home of Professional Open Source
-- Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
-- contributors by the @authors tag. See the copyright.txt in the
-- distribution for a full listing of individual contributors.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- http://www.apache.org/licenses/LICENSE-2.0
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- You can use this file to load seed service into the database using SQL statements

insert into role(role_profile) VALUES ('ADMIN');
insert into role(role_profile) VALUES ('USER');

-- Do not insert users with SQL, since the password has to be hashed
-- insert into user(username, email, password, user_group_id) VALUES ('szhao', 's.zhao@opengate.biz', 'hello123!', 1);
-- insert into user(username, email, password, user_group_id) VALUES ('clarice', 'clarice@gmail.com', 'hao123!', 2);

INSERT INTO university(full_name, homepage, student_email_postfix, teacher_email_postfix) VALUES ('Politecnico di Torino', 'www.polito.it', '@studenti.polito.it', '@polito.it');
INSERT INTO university(full_name, homepage, student_email_postfix, teacher_email_postfix) VALUES ('Politecnico di Milano', 'www.polimi.it', '@polimi.it', '@politi.it');

INSERT INTO course(code, title, description, university_id) VALUES ('CS01', 'c and algorithm', 'some description', 1);
INSERT INTO course(code, title, description, university_id) VALUES ('CS02', 'Java OOP', 'some description', 1);

insert into programming_language(name, compile_command) VALUES ('C', 'gcc -Wall ');
insert into programming_language(name, compile_command) VALUES ('Java', 'javac ');

INSERT INTO question(description, function_declaration, reference_solution, title, solution_free, course_code) VALUES ('Define a tree structure and implement the insert function.', 'void insert(int a, tree_t **h){}', 'todo', 'tree insert', true, 'CS01');

INSERT INTO question(description, function_declaration, reference_solution, title, solution_free, course_code) VALUES ('Define a list structure and implement the insert function.', 'void insert(int a, list_t **h){}', 'todo', 'list insert', true, 'CS01');