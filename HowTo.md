# Java EE Application with Maven and WildFly 
The procedures presented in this post apply to macOS Sierra 10.12.5, JDK 1.8.This tutorial is not a Java beginner tutorial, so make sure you have development tools installed, i.e. JDK, brew, IntelliJ IDEA etc.  
## Install WildFly
Install WildFly with brew
```
brew install wildfly-as
```
And configure the environment variable `JBOSS_HOME` in `~/.bash_profile` by adding the following content
```
export JBOSS_HOME=/usr/local/opt/wildfly-as/libexec
export PATH=${PATH}:${JBOSS_HOME}/bin
```
Run `source .bash_profile` and start WildFly
```
 brew services start wildfly-as
```
If you are behind a proxy, you may need to add proxy settings for each brew command, like 
```
ALL_PROXY=192.169.2.58:8080 brew [commands]
``` 
## Create project with Maven archetype 
Run `mvn archetype:generate`, a list of archetypes will show up and filter with `ear`, I choose 
```
17: remote -> org.wildfly.archetype:wildfly-javaee7-webapp-ear-archetype (An archetype that generates a starter Java EE 7 webapp project for JBoss Wildfly. The project is an EAR, with an EJB-JAR and WAR)
```
After entering your group ID, artefact ID and package name, etc a Maven project will be created. Here I use 
```
groupId: com.codeora
artifactId: barclan
version: 1.0-SNAPSHOT
package: com.codeora
```
A directory named `barclan` is created with contents 
```
README.md	
barclan-ear	
barclan-ejb	
barclan-web	
pom.xml
```
## Deploy 
Go to project base directory, namely `barclan` and run 
```
mvn clean install
```
Check if the WildFly is running if there is any problem. Now deploy
```
mvn wildfly:deploy
```
Now it’s possible to visit the page on `http://localhost:8080/barclan-web`. 







