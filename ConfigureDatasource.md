# Configure WildFly MySQL Data Source

- Download MySQL connector `mysql-connector-java-5.1.42-bin.jar` to directory *$JBOSS_HOME/modules/system/layers/base/com/mysql/*

- Create `module.xml` under the same directory *$JBOSS_HOME/modules/system/layers/base/com/mysql/*

```
<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.3" name="com.mysql">
  <resources>
    <resource-root path="mysql-connector-java-5.1.42-bin.jar"/>
  </resources>
  <dependencies>
    <module name="javax.api"/>
    <module name="javax.transaction.api"/>
  </dependencies>
</module>
```

- Add the following code to `$JBOSS_HOME/standalone/standalone.xml`. Note that the database name, username and password needs to be changed. And remember to create the corresponding database!

```
<datasources>
    <datasource jndi-name="java:jboss/MySqlDS" pool-name="MySqlDS">
        <connection-url>jdbc:mysql://localhost:3306/fooclan</connection-url>
        <driver>mysql</driver>
        <security>
            <user-name>fooclan</user-name>
            <password>fooclan</password>
        </security>
        <validation>
            <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"></valid-connection-checker>
             <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter"></exception-sorter>
         </validation>
  </datasource>
  <drivers>
    <driver name="mysql" module="com.mysql">
        <xa-datasource-class>com.mysql.jdbc.jdbc2.optional.MysqlXADataSource</xa-datasource-class>
     </driver>
  </drivers>
</datasources>
```

- Goto *http://localhost:9990/console* choose *Configuration -> Subsystems -> Datasources -> Non-XA -> MySqlDS -> Test Connection*. If test succeeds you can use that datasource now.

## Using the Maven WildFly plugin
If you are in a maven project with the WildFly plugin, **you do not need to add the datasource information in the** `standalone.xml`, you can put the configuration in the configuration file `*-ds.xml`.

```
<datasources>
    <datasource jndi-name="java:jboss/MySqlDS" pool-name="MySqlDS">
        <connection-url>jdbc:mysql://localhost:3306/fooclan</connection-url>
        <driver>mysql</driver>
        <security>
            <user-name>fooclan</user-name>
            <password>fooclan</password>
        </security>
        <validation>
            <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"></valid-connection-checker>
             <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter"></exception-sorter>
         </validation>
  </datasource>
```
And in the `persistence.xml` use the Datasource

```
<persistence version="2.1"
   xmlns="http://xmlns.jcp.org/xml/ns/persistence" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="
        http://xmlns.jcp.org/xml/ns/persistence
        http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">
   <persistence-unit name="primary">
      <!-- The datasource is deployed as <EAR>/META-INF/fooclan-ds.xml, you
         can find it in the source at ear/src/main/application/META-INF/fooclan-ds.xml -->
      <jta-data-source>java:jboss/MySqlDS</jta-data-source>
      <properties>
         <!-- Properties for Hibernate -->
         <property name="hibernate.hbm2ddl.auto" value="create-drop" />
         <property name="hibernate.show_sql" value="true" />
      </properties>
   </persistence-unit>
</persistence>
```
Open http://localhost:8080/fooclan-web, add new user in Member Registration. After that, you can check if the user is added to table `Registrant`
```
MariaDB [fooclan]> select * from Registrant;
+----+---------------------------+------------+--------------+
| id | email                     | name       | phone_number |
+----+---------------------------+------------+--------------+
|  0 | john.smith@mailinator.com | John Smith | 2125551212   |
|  1 | s.zhao@opengate.biz       | Shilong    | 3333333333   |
+----+---------------------------+------------+--------------+
2 rows in set (0.00 sec)
```

References:
[1] https://tomylab.wordpress.com/2016/07/24/how-to-add-a-datasource-to-wildfly/
[2] https://access.redhat.com/documentation/en-US/JBoss_Enterprise_Application_Platform/6/html/Administration_and_Configuration_Guide/Example_MySQL_Datasource1.html
[3] https://gitlab.com/shilongzhao/javaee-maven
